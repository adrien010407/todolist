package fr.orleans.m1.todolist.model;

import android.net.Uri;
import android.util.Log;
import android.util.Pair;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Calendar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.util.NamingHelper;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import fr.orleans.m1.todolist.R;

public class Task extends SugarRecord implements Serializable {
    @Ignore
    private final static String scheme = "http";
    @Ignore
    private final static String host = "todolist.m1.orleans.fr";
    @Ignore
    public final static String type = "task";
    @Ignore
    private final static int priorityIds[] = {R.drawable.ic_circle_green_24dp, R.drawable.ic_circle_orange_24dp, R.drawable.ic_circle_red_24dp};

    @Expose
    private String name = "";
    @Expose
    private String description = "";
    private long createdAt = 0;
    @Expose
    private long deadline = 0;
    @Ignore
    @Expose
    private List<Task> subTasks = new ArrayList<>();

    @Expose
    private int priority = 0;
    @Ignore
    @Expose
    private Set<User> users = new HashSet<>();
    @Ignore
    private List<TaskUsers> addedUsers = new ArrayList<>();
    @Ignore
    private List<TaskUsers> deletedUsers = new ArrayList<>();
    private User creator = null;
    private User master = null;
    @Expose
    private int advancement = 0;
    private Task parent = null;

    /*
        TODO: Database connection (sugarORM ?)
        TODO: Constructors and methods required by orm
        TODO: Other constructors
        TODO: Setters
     */

    public Task() {

    }

    public Task(String name, String description, long createdAt, long deadline, List<Task> subTasks, int priority, Set<User> users, User creator, User master, int advancement) {
        setName(name);
        setDescription(description);
        setCreatedAt(createdAt);
        setDeadline(deadline);
        setSubTasks(subTasks);
        setPriority(priority);
        setUsers(users);
        setCreator(creator);
        setMaster(master);
        setAdvancement(advancement);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getDeadline() {
        return deadline;
    }

    public Pair<Boolean, String> getDeadlineDelay() {
        String d = "X"; boolean late = false;
        long delay = (deadline == 0) ? 0 : deadline - System.currentTimeMillis();

        if (deadline != 0 && delay < 0) {
            late = true;
            delay *= -1;
        }

        Calendar cDelay = Calendar.getInstance();
        cDelay.setTimeInMillis(delay);

        if (cDelay.get(Calendar.YEAR) - 1970 > 0) {
            d = String.format(Locale.FRANCE, "%da", cDelay.get(Calendar.YEAR) - 1970);
        } else if (cDelay.get(Calendar.MONTH) > 0) {
            d = String.format(Locale.FRANCE, "%dm", cDelay.get(Calendar.MONTH));
        } else if (cDelay.get(Calendar.DAY_OF_MONTH) > 1) {
            d = String.format(Locale.FRANCE, "%dj", cDelay.get(Calendar.DAY_OF_MONTH) - 1);
        } else if (cDelay.get(Calendar.HOUR_OF_DAY) > 1) {
            d = String.format(Locale.FRANCE, "%dh", cDelay.get(Calendar.HOUR_OF_DAY) - 1);
        } else if (cDelay.get(Calendar.MINUTE) > 0) {
            d = String.format(Locale.FRANCE, "%dmn", cDelay.get(Calendar.MINUTE));
        }
        return new Pair<>(late, d);
    }

    public List<Task> getSubTasks() {
        if (this.getId() != null) {
            this.subTasks = Task.find(Task.class, "parent = ?", new String[] {this.getId().toString()}, "", "priority desc", "");
        }
        return subTasks;
    }

    public int getPriority() {
        if (priority == -1) priority = 0;
        return priority;
    }

    public int getPriorityResource() {
        return priorityIds[getPriority()];
    }

    public Set<User> getUsers() {
        loadUsers();
        return users;
    }

    public User getCreator() {
        return creator;
    }

    public User getMaster() {
        return master;
    }

    public int getAdvancement() {
        return advancement;
    }

    public Task getParent() {
        return parent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public void setSubTasks(List<Task> subTasks) {
        this.subTasks = subTasks;
        for (Task t : subTasks) {
            t.setParent(this);
        }
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setMaster(User master) {
        this.master = master;
    }

    public void setAdvancement(int advancement) {
        this.advancement = advancement;
    }

    public void setParent(Task parent) {
        this.parent = parent;
    }

    @Override
    public boolean delete() {
        boolean res = true;
        for (Task t : subTasks) {
            res = res & t.delete();
        }
        for (TaskUsers tu : TaskUsers.find(TaskUsers.class, "task = ?", getId().toString())) {
            res = res & tu.delete();
        }
        return res & super.delete();
    }

    @Override
    public long save() {
        long id = super.save();
        for (Task t : subTasks) {
            t.save();
        }
        for (TaskUsers tu : deletedUsers) {
            tu.delete();
        }
        for (User u : getUsers()) {
            TaskUsers tu = TaskUsers.getTaskUsers(this, User.getUser(u.getUsername()));
            tu.save();
        }
        return id;
    }

    @Override
    public String toString() {
        return String.format(Locale.FRANCE, "Task:%s", name);
    }

    public void addUser(User newpart) {
        users.add(newpart);
    }

    public void removeUser(User newpart) {
        users.remove(newpart);
        TaskUsers tu = TaskUsers.getTaskUsers(this, newpart);
        deletedUsers.add(tu);
    }

    private void loadUsers() {
        if (getId() != null) {
            for (TaskUsers tu : TaskUsers.find(TaskUsers.class, "task = ?", getId().toString())) {
                users.add(tu.getUser());
            }
        }
        for (TaskUsers tu : deletedUsers) {
            users.remove(tu.getUser());
        }
    }

    public Uri genUri() {
        String url = scheme + "://" + host + "/" + type + "?";
        try {
            url += "json=" + URLEncoder.encode(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Uri uri = Uri.parse(url);
        return uri;
    }

    public static Task loadUri(Uri uri) {
        Task task;
        if (uri.getPathSegments().get(0).equals(type)) {
            String name; String description; Long deadline; Integer priority; Integer advancement; String subTasks;

            task = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(uri.getQueryParameter("json"), Task.class);

            return task;
        }
        return null;
    }

    public void clearUsers() {
        users.clear();
    }
}
