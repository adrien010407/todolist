package fr.orleans.m1.todolist.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

import fr.orleans.m1.todolist.R;

public class AdvancementBar extends RelativeLayout {
    // La SeekBar
    private SeekBar sb_adv;
    // Le texte affichant la progression
    private TextView tv_adv;

    private boolean editable = false;

    public AdvancementBar(Context context) {
        super(context);
        init();
    }

    public AdvancementBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AdvancementBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public AdvancementBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.avancement_bar, this);

        sb_adv = findViewById(R.id.sb_adv);
        tv_adv = findViewById(R.id.tv_adv);


        sb_adv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return !editable;
            }
        });
        sb_adv.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateTextValue();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void updateTextValue(int progress) {
        tv_adv.setText(String.format(Locale.FRANCE, "%d%%", progress));
    }

    private void updateTextValue() {
        tv_adv.setText(String.format(Locale.FRANCE, "%d%%", sb_adv.getProgress()));
    }

    public int getProgress() {
        return sb_adv.getProgress();
    }

    public void setProgress(int progress) {
        updateTextValue(progress);
        sb_adv.setProgress(progress);
    }

    public Drawable getThumb() {
        return sb_adv.getThumb();
    }

    public void setEditable(boolean e) {
        editable = e;
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener(l);
        sb_adv.setOnClickListener(l);
        tv_adv.setOnClickListener(l);
    }
}
