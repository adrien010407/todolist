package fr.orleans.m1.todolist;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

interface SelectableTaskActivity {

    public void setModeToSelection();

    public void setModeToShow();

    public Context getContext();
}
