package fr.orleans.m1.todolist;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

public class TodoListAppWidgetProvider extends AppWidgetProvider {
    public final static String TASK_ACTION = "fr.orleans.m1.todolist.TASK_ACTION";
    public final static String TASK_ID = "fr.orleans.m1.todolist.TASK_ID";

    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        if (intent.getAction().equals(TASK_ACTION)) {
            int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            long taskId = intent.getLongExtra(TASK_ID, 0);
            Intent iTaskActivity = new Intent(context, TaskActivity.class);
            iTaskActivity.putExtra("task", taskId);

            context.startActivity(iTaskActivity);
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;

        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];

            Intent intent = new Intent(context, TaskListWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.todolist_appwidget);
            views.setRemoteAdapter(R.id.tasklist, intent);
            views.setEmptyView(R.id.tasklist, R.id.empty_view);

            Intent taskIntent = new Intent(context, TodoListAppWidgetProvider.class);
            taskIntent.setAction(TASK_ACTION);
            taskIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
            PendingIntent taskPendingIntent = PendingIntent.getBroadcast(context, 0, taskIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setPendingIntentTemplate(R.id.tasklist, taskPendingIntent);

            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }
}
