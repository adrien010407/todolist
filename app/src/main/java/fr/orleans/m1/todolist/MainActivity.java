package fr.orleans.m1.todolist;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import fr.orleans.m1.todolist.model.Task;
import fr.orleans.m1.todolist.model.TaskUsers;
import fr.orleans.m1.todolist.model.User;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private ViewPagerAdapter<TaskListFragment> adapter;
    private Toolbar toolbar;
    private TabLayout tabLayout;

    private static final int MODE_SELECTION = 1;
    private static final int MODE_SHOW = 2;

    public static List<Task> creerTmpListeTaches() {
        List<Task> tmpListeTaches = new ArrayList<>();
        Calendar deadline = Calendar.getInstance();
        deadline.set(2018,11,5,16,30,0);
        tmpListeTaches.add(new Task(
                "Tâche 1",
                "La première tâche",
                System.currentTimeMillis(),
                deadline.getTimeInMillis(),
                new ArrayList<Task>(),
                1,
                new HashSet<User>(),
                null,
                null,
                30
        ));
        List<Task> sous_liste = new ArrayList<>();
            deadline.set(2019,0,3,0,0,0);
            sous_liste.add(new Task(
                    "Développement",
                    "Continuer l'application",
                    System.currentTimeMillis(),
                    deadline.getTimeInMillis(),
                    new ArrayList<Task>(),
                    1,
                    new HashSet<User>(),
                    null,
                    null,
                    20
            ));
            deadline.set(2019,0,5,0,0,0);
            sous_liste.add(new Task(
                    "Tests",
                    "Faire fonctionner l'application",
                    System.currentTimeMillis(),
                    deadline.getTimeInMillis(),
                    new ArrayList<Task>(),
                    0,
                    new HashSet<User>(),
                    null,
                    null,
                    0
            ));
        deadline.set(2019,0,5,12,15,0);
        tmpListeTaches.add(new Task(
                "Créer l'application TODO List",
                "Créer une application de TODO List en Android\nListe des spécifications gardé par Baptiste",
                System.currentTimeMillis(),
                deadline.getTimeInMillis(),
                sous_liste,
                2,
                new HashSet<User>(),
                null,
                null,
                10
        ));
        tmpListeTaches.add(new Task(
                "Regarder Doctor Who",
                "",
                System.currentTimeMillis(),
                0,
                new ArrayList<Task>(),
                0,
                new HashSet<User>(),
                null,
                null,
                20
        ));
        tmpListeTaches.add(new Task(
                "Amener le chaos sur le monde",
                "En tant qu'apôtre du chaos, ton devoir est d'amener le chaos infini sur le monde des humains inférieur.\nQue ta mission soit menée de succés.\nEl Psy Congroo.",
                System.currentTimeMillis(),
                0,
                new ArrayList<Task>(),
                2,
                new HashSet<User>(),
                null,
                null,
                50
        ));
        return tmpListeTaches;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = getIntent();
        if (i.getAction() != null && i.getAction().equals(Intent.ACTION_VIEW)) {
            Uri uri = i.getData();
            if (uri != null && uri.getPathSegments().get(0).equals(Task.type)) {
                i.setClass(this, TaskActivity.class);
                startActivity(i);
            }
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // Il y a deux tabs, donc on peut utiliser xor
                adapter.getItem(position ^ 1).isSelectedToShow();
            }
        });
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter<>(getSupportFragmentManager());
        adapter.addFragment(TaskListFragment.getTaskListFragment("parent = 0 and advancement < 100"), "en cours");
        adapter.addFragment(TaskListFragment.getTaskListFragment("parent = 0 and advancement = 100"), "terminées");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        adapter.getItem(viewPager.getCurrentItem()).onCreateOptionsMenu(menu, getMenuInflater());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return adapter.getItem(viewPager.getCurrentItem()).onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!adapter.getItem(viewPager.getCurrentItem()).isSelectedToShow()) {
            super.onBackPressed();
        }
    }
}
