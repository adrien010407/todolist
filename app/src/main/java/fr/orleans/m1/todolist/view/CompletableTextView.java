package fr.orleans.m1.todolist.view;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatAutoCompleteTextView;

public class CompletableTextView extends AppCompatAutoCompleteTextView {
    public CompletableTextView(Context context) {
        super(context);
    }

    public CompletableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CompletableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }
}
