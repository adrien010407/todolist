package fr.orleans.m1.todolist;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.widget.ImageView;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import fr.orleans.m1.todolist.model.Task;
import fr.orleans.m1.todolist.model.TaskUsers;
import fr.orleans.m1.todolist.model.User;
import fr.orleans.m1.todolist.view.CompletableTextView;

public class ParticipantDialogFragment extends DialogFragment {
    private final static String ARG_ID = "id";
    private final static String ARG_TYPE = "isNew";
    private final static String ARG_UNAME = "username";
    private TaskActivity a;

    public ParticipantDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static ParticipantDialogFragment newInstance(Task task, boolean isNew, String username) {
        ParticipantDialogFragment frag = new ParticipantDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ID, task);
        args.putBoolean(ARG_TYPE, isNew);
        args.putString(ARG_UNAME, username);
        frag.setArguments(args);
        return frag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        a = ((TaskActivity)requireActivity());

        AlertDialog.Builder builder = new AlertDialog.Builder(a);

        LayoutInflater inflater = a.getLayoutInflater();

        View rootView = inflater.inflate(R.layout.fragment_participant_dialog, null);
        builder.setView(rootView);

        final CompletableTextView actv_part = rootView.findViewById(R.id.actv_part);
        final ArrayAdapter<User> aa_user = new ArrayAdapter<User>(a, android.R.layout.select_dialog_item, User.findWithQuery(User.class, "SELECT * FROM USER"));
        actv_part.setAdapter(aa_user);
        actv_part.setText(getArguments().getString(ARG_UNAME));

        ImageView delete = rootView.findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeOldPart(actv_part, getArguments().getString(ARG_UNAME));
                a.loadUsers();
                a.notifyUsersChange();
                ParticipantDialogFragment.this.getDialog().cancel();
            }
        });

        if (getArguments().getBoolean(ARG_TYPE) || getArguments().getString(ARG_UNAME).equals("")) {
            //Ajouter
            builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    addNewPart(actv_part);
                    TaskActivity a = ((TaskActivity)getActivity());
                    a.loadUsers();
                    a.notifyUsersChange();
                }
            });
        } else {
            //Modifier
            builder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    removeOldPart(actv_part, getArguments().getString(ARG_UNAME));
                    addNewPart(actv_part);
                    TaskActivity a = ((TaskActivity)getActivity());
                    a.loadUsers();
                    a.notifyUsersChange();
                }
            });
        }

        builder.setNegativeButton(R.string.cancel_action, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ParticipantDialogFragment.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

    private void removeOldPart(AppCompatAutoCompleteTextView actv_part, String string) {
        Task t = (Task) getArguments().getSerializable(ARG_ID);
        User toRemove = null;
        for (User u : t.getUsers()) {
            if ((u != null && u.getCanonicalUsername().equals(User.toCanonical(string))) || (u == null && string == "")) {
                toRemove = u;
                break;
            }
        }
        t.removeUser(toRemove);
    }

    private void addNewPart(AppCompatAutoCompleteTextView actv_part) {
        Task t = (Task) getArguments().getSerializable(ARG_ID);
        List<User> list = User.find(User.class, "canonical_username = ?", User.toCanonical(actv_part.getText().toString()));
        User newpart;
        //list est de taille 1 ou 0, puisque canonical_username est unique
        if (list.isEmpty()) {
            //Le participant n'existe pas encore, on le créée
            newpart = User.getUser(actv_part.getText().toString());
            newpart.save();
            t.addUser(newpart);
        } else {
            newpart = list.get(0);
            t.addUser(newpart);
        }
    }
}
