package fr.orleans.m1.todolist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import fr.orleans.m1.todolist.model.Task;
import fr.orleans.m1.todolist.model.User;
import fr.orleans.m1.todolist.view.AdvancementBar;
import fr.orleans.m1.todolist.view.CustomLinearLayoutManager;
import fr.orleans.m1.todolist.view.DividerItemDecorator;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private TaskActivity context;

    private List<User> user_list;

    private Set<UserViewHolder> viewHolderSet = new HashSet<>();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout mLayout;
        public ImageView mIcon;
        public TextView mTextView;

        public UserViewHolder(LinearLayout l) {
            super(l);
            mLayout = l;
            mTextView = l.findViewById(android.R.id.text1);
            mIcon = l.findViewById(android.R.id.icon);
        }
    }

    public UserAdapter(TaskActivity c, List<User> ul) {
        context = c;
        user_list = ul;
    }

    @NonNull
    @Override
    public UserAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.activity_list_item, viewGroup, false);

        UserViewHolder vh = new UserViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final UserAdapter.UserViewHolder viewHolder, final int pos) {
        final User user = user_list.get(pos);

        viewHolderSet.add(viewHolder);

        if (user != null) {
            viewHolder.mTextView.setText(user.getUsername());

        }
        viewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(user, viewHolder);
            }
        });
        viewHolder.mIcon.setImageResource(R.drawable.ic_person_black_24dp);

    }

    private void click(User user, UserViewHolder v) {
        String username = (user != null) ? user.getUsername() : "";
        context.updateParticipant(username);
    }

    @Override
    public int getItemCount() {
        return user_list.size();
    }

    public List<User> getUsers() {
        return user_list;
    }
}
