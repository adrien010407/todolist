package fr.orleans.m1.todolist;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import fr.orleans.m1.todolist.model.Task;
import fr.orleans.m1.todolist.model.User;
import fr.orleans.m1.todolist.view.AdvancementBar;

public class TaskActivity extends AppCompatActivity implements SelectableTaskActivity {
    private final static String CAT_TASK = "fr.orleans.m1.todolist.category.TASK";
    private ShareActionProvider mShareActionProvider;

    private final static int DATE = 0;
    private final static int TIME = 1;
    private Task task;

    private Calendar deadline_calendar;
    private DatePickerDialog.OnDateSetListener deadline_picker_date;
    private TimePickerDialog.OnTimeSetListener deadline_picker_time;
    private EditText et_title;

    private EditText et_deadline_date;
    private EditText et_deadline_time;
    private Button b_clearDate;

    private EditText et_description;

    private RadioGroup rg_prio;

    private AdvancementBar sb_adv;

    private RecyclerView rv_parts;
    private List<User> users_list = new ArrayList<>();
    private UserAdapter rv_user_adapter;

    private RecyclerView rv_subtasks;
    private List<Task> tasks_list = new ArrayList<>();
    private TaskAdapter rv_task_adapter;

    private LinearLayout ll_new_part;
    private LinearLayout ll_new_subtask;

    private Integer priorityIds[] = {R.id.prio_low, R.id.prio_med, R.id.prio_high};

    private ActionBar actionBar;
    private boolean edition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        Intent i = getIntent();

        if (i.getAction() != null && i.getAction().equals(Intent.ACTION_VIEW)) {
            Uri uri = i.getData();
            if (uri != null && uri.getPathSegments().get(0).equals(Task.type)) {
                task = Task.loadUri(uri);
                if (task == null) {
                    task = new Task();
                }
            } else {
                task = new Task();
            }
            edition = true;
        } else {
            long taskId = i.getLongExtra("task", -1);
            if (taskId == -1) {
                task = new Task();
            } else {
                task = Task.findById(Task.class, taskId);
            }
            edition = i.getBooleanExtra("isedit", false);
            long parent = i.getLongExtra("parent", 0); // Les id dans sugarORM commencent à 1
            if (parent != 0) {
                task.setParent(Task.findById(Task.class, parent));
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);

        actionBar.setTitle(null);

        actionBar.setDisplayUseLogoEnabled(false);

        et_title = findViewById(R.id.title);

        et_description = findViewById(R.id.desc);

        et_deadline_date = findViewById(R.id.et_deadline_date);
        et_deadline_time = findViewById(R.id.et_deadline_time);

        b_clearDate = findViewById(R.id.b_clearDate);

        rg_prio = findViewById(R.id.prio_group);

        sb_adv = findViewById(R.id.ab_adv);

        rv_subtasks = findViewById(R.id.subTaskList);

        rv_parts = findViewById(R.id.participantList);

        ll_new_part = findViewById(R.id.ll_new_part);

        ll_new_subtask = findViewById(R.id.ll_new_subtask);

        load();

        changeMode(edition);
    }

    private void updateLabel(int type) {
        if (type == DATE) {
            String dateFormat = (deadline_calendar.getTimeInMillis() != 0) ? "dd/MM/yy" : ""; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.FRANCE);

            et_deadline_date.setText(sdf.format(deadline_calendar.getTime()));
        } else if (type == TIME) {
            String timeFormat = (deadline_calendar.getTimeInMillis() != 0) ? "H:mm" : "";
            SimpleDateFormat sdf = new SimpleDateFormat(timeFormat, Locale.FRANCE);

            et_deadline_time.setText(sdf.format(deadline_calendar.getTime()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Création du menu
        int res;
        if (rv_task_adapter.isSelectionMode()) {
            res = R.menu.delete_share_cancel;
            getMenuInflater().inflate(res, menu);
        } else if (edition) {
            res = R.menu.save_cancel;
            getMenuInflater().inflate(res, menu);
        } else {
            res = R.menu.basic_editing;
            getMenuInflater().inflate(res, menu);
            MenuItem item = menu.findItem(R.id.action_share);

            mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            Uri uri = task.genUri();
            Log.d("TL_TA_uri", uri.toString());
            shareIntent.putExtra(Intent.EXTRA_TEXT, uri.toString());
            Log.d("TL_TA_extra", shareIntent.getStringExtra(Intent.EXTRA_TEXT));
            shareIntent.setType("text/plain");
            setShareIntent(shareIntent);
        }
        return true;
    }

    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    public void datePick(View view) {
        if (edition) {
            Calendar tmpC = deadline_calendar;

            if (tmpC.getTimeInMillis() == 0) {
                tmpC.setTimeInMillis(System.currentTimeMillis());
            }
            new DatePickerDialog(
                    TaskActivity.this,
                    deadline_picker_date,
                    tmpC.get(Calendar.YEAR),
                    tmpC.get(Calendar.MONTH),
                    tmpC.get(Calendar.DAY_OF_MONTH)
            ).show();
        }
    }

    public void timePick(View view) {
        if (edition) {
            Calendar tmpC = deadline_calendar;

            if (tmpC.getTimeInMillis() == 0) {
                tmpC.setTimeInMillis(System.currentTimeMillis());
            }
            new TimePickerDialog(
                    TaskActivity.this,
                    deadline_picker_time,
                    tmpC.get(Calendar.HOUR_OF_DAY),
                    tmpC.get(Calendar.MINUTE),
                    true
            ).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                createSubTask(null);
                return true;

            case R.id.action_edit:
                changeMode(true);
                invalidateOptionsMenu();
                return true;

            case R.id.action_delete: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Suppression");
                builder.setMessage("Voulez-vous vraiment supprimer la tâche ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        task.delete();
                        finish();
                    }
                });

                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
                return true;
            }

            case R.id.action_save:
                changeMode(false);
                invalidateOptionsMenu();
                save();
                task.save();
                return true;

            case R.id.action_cancel:
                changeMode(false);
                invalidateOptionsMenu();
                load();
                return true;

            case R.id.action_task_cancel:
                rv_task_adapter.setModeToShow();
                return true;

            case R.id.action_task_delete: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Suppression");
                builder.setMessage("Voulez-vous vraiment supprimer les tâches séléctionnées ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rv_task_adapter.deleteSelectedTask();
                        rv_task_adapter.notifyDataSetChanged();
                        rv_task_adapter.setModeToShow();
                    }
                });

                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void createSubTask(View view) {
        Intent iTaskActivity = new Intent(this, TaskActivity.class);
        iTaskActivity.putExtra("task", -1);
        iTaskActivity.putExtra("isedit", true);
        Log.d("TaskActivitySubTask", task.getId().toString());
        iTaskActivity.putExtra("parent", task.getId());

        this.startActivity(iTaskActivity);
    }

    private void changeMode(boolean e) {
        edition = e;

        if (edition) {
            TypedValue tv_editTextBackground = new TypedValue();
            getTheme().resolveAttribute(R.attr.editTextBackground, tv_editTextBackground, true);

            et_title.setBackgroundResource(tv_editTextBackground.resourceId);
            et_title.setFocusable(true);
            et_title.setFocusableInTouchMode(true);

            et_description.setBackgroundResource(tv_editTextBackground.resourceId);
            et_description.setFocusable(true);
            et_description.setFocusableInTouchMode(true);

            et_deadline_date.setBackgroundResource(tv_editTextBackground.resourceId);

            et_deadline_time.setBackgroundResource(tv_editTextBackground.resourceId);

            b_clearDate.setEnabled(true);

            for (int i = 0; i < rg_prio.getChildCount(); i++) {
                rg_prio.getChildAt(i).setEnabled(true);
            }

            sb_adv.getThumb().setTint(getResources().getColor(R.color.colorPrimaryLight));

            ll_new_part.setVisibility(View.VISIBLE);
            ll_new_subtask.setVisibility(View.VISIBLE);
        } else {
            et_title.setBackgroundResource(R.drawable.edit_text_background_transparent);
            et_title.setFocusable(false);
            et_title.setFocusableInTouchMode(false);

            et_description.setBackgroundResource(R.drawable.edit_text_background_transparent);
            et_description.setFocusable(false);
            et_description.setFocusableInTouchMode(false);

            et_deadline_date.setBackgroundResource(R.drawable.edit_text_background_transparent);
            et_description.setFocusable(false);
            et_deadline_date.setFocusableInTouchMode(false);

            et_deadline_time.setBackgroundResource(R.drawable.edit_text_background_transparent);
            et_description.setFocusable(false);
            et_deadline_time.setFocusableInTouchMode(false);

            b_clearDate.setEnabled(false);

            for (int i = 0; i < rg_prio.getChildCount(); i++) {
                rg_prio.getChildAt(i).setEnabled(false);
            }

            sb_adv.getThumb().setTint(getResources().getColor(android.R.color.transparent));

            ll_new_part.setVisibility(View.GONE);
            ll_new_subtask.setVisibility(View.GONE);
        }
        sb_adv.setEditable(edition);
    }

    private void load() {
        et_title.setText(task.getName());
        et_description.setText(task.getDescription());

        deadline_calendar = Calendar.getInstance();
        deadline_calendar.setTimeInMillis(task.getDeadline());

        updateLabel(DATE);
        updateLabel(TIME);

        deadline_picker_date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                deadline_calendar.set(Calendar.YEAR, year);
                deadline_calendar.set(Calendar.MONTH, monthOfYear);
                deadline_calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(DATE);
            }

        };
        deadline_picker_time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                deadline_calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                deadline_calendar.set(Calendar.MINUTE, minute);
                updateLabel(TIME);
            }
        };

        rg_prio.check(priorityIds[task.getPriority()]);

        sb_adv.setProgress(task.getAdvancement());

        RecyclerView.LayoutManager rv_layout_manager = new LinearLayoutManager(this);
        rv_subtasks.setLayoutManager(rv_layout_manager);

        DividerItemDecoration rv_divider_i_deco = new DividerItemDecoration(rv_subtasks.getContext(), DividerItemDecoration.VERTICAL);
        rv_subtasks.addItemDecoration(rv_divider_i_deco);

        loadTasks();
        rv_task_adapter = new TaskAdapter(this, tasks_list);
        rv_subtasks.setAdapter(rv_task_adapter);

        RecyclerView.LayoutManager rv_user_layout_manager = new LinearLayoutManager(this);
        rv_parts.setLayoutManager(rv_user_layout_manager);

        loadUsers();
        rv_user_adapter = new UserAdapter(this, users_list);
        rv_parts.setAdapter(rv_user_adapter);
    }

    public void loadTasks() {
        tasks_list.clear();
        tasks_list.addAll(task.getSubTasks());
    }

    public void loadUsers() {
        users_list.clear();
        users_list.addAll(task.getUsers());
    }

    private void save() {
        task.setName(et_title.getText().toString());
        task.setDescription(et_description.getText().toString());
        Log.d("TaskActivitySave", "deadline_calendar = " + deadline_calendar.toString());
        task.setDeadline(deadline_calendar.getTimeInMillis());
        task.setPriority(Arrays.asList(priorityIds).indexOf(rg_prio.getCheckedRadioButtonId()));
        task.setAdvancement(sb_adv.getProgress());
        task.setSubTasks(rv_task_adapter.getTasks());
    }

    @Override
    public void setModeToSelection() {
        invalidateOptionsMenu();
    }

    @Override
    public void setModeToShow() {
        invalidateOptionsMenu();
    }

    @Override
    public Context getContext() {
        return this;
    }

    public void clearDateTime(View view) {
        deadline_calendar.setTimeInMillis(0);
        updateLabel(DATE);
        updateLabel(TIME);
    }

    public void goHome(View view) {
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        if (NavUtils.shouldUpRecreateTask(this, upIntent) || isTaskRoot()) {
            TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
        } else {
            NavUtils.navigateUpTo(this, upIntent);
        }
    }
    public void goBack(View view) {
        onBackPressed();
    }

    public void addParticipant(View view) {
        if (edition) {
            FragmentManager fm = getSupportFragmentManager();
            ParticipantDialogFragment.newInstance(task, true, "").show(fm, "add_part");
        }
    }

    public void updateParticipant(String oldUsername) {
        if (edition) {
            FragmentManager fm = getSupportFragmentManager();
            ParticipantDialogFragment.newInstance(task, false, oldUsername).show(fm, "update_part");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadTasks();
        rv_task_adapter.notifyDataSetChanged();
    }

    public void notifyUsersChange() {
        rv_user_adapter.notifyDataSetChanged();
    }
    public void notifyTasksChange() {
        rv_task_adapter.notifyDataSetChanged();
    }
}
