package fr.orleans.m1.todolist;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fr.orleans.m1.todolist.model.Task;
import fr.orleans.m1.todolist.model.User;
import fr.orleans.m1.todolist.view.DividerItemDecorator;

public class TaskListFragment extends Fragment implements SelectableTaskActivity {
    public static final String ARG_WH = "whereOptions";

    private RecyclerView rv_task_list;
    private TaskAdapter rv_adapter;
    private RecyclerView.LayoutManager rv_layout_manager;
    private RecyclerView.ItemDecoration rv_divider_item_decoration;

    // ArrayList de Test pour créer des tâches à partir de nombres
    private List<Task> listeTaches = new ArrayList<>();

    public static TaskListFragment getTaskListFragment(String whereOptions) {
        TaskListFragment f = new TaskListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_WH, whereOptions);
        f.setArguments(args);
        return f;
    }



    public TaskListFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        chargerBD();

        rv_layout_manager = new LinearLayoutManager(getContext());

        rv_adapter = new TaskAdapter(this, listeTaches);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_list, container, false);
        Bundle args = getArguments();

        rv_task_list = rootView.findViewById(R.id.rv_task_list);

        rv_task_list.setLayoutManager(rv_layout_manager);

        rv_divider_item_decoration = new DividerItemDecorator(rv_task_list.getContext());
        rv_task_list.addItemDecoration(rv_divider_item_decoration);

        rv_task_list.setAdapter(rv_adapter);

        return rootView;
    }

    private void chargerBD() {
        Bundle args = getArguments();
        listeTaches.clear();
        listeTaches.addAll(Task.find(Task.class, args.getString(ARG_WH), new String[0], "", "priority desc", ""));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Création du menu
        if (rv_adapter != null && rv_adapter.isSelectionMode()) {
            inflater.inflate(R.menu.delete_share_cancel, menu);
        } else {
            inflater.inflate(R.menu.main_menu, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                Intent iTaskActivity = new Intent(getActivity(), TaskActivity.class);
                iTaskActivity.putExtra("task", -1);
                iTaskActivity.putExtra("isedit", true);

                this.startActivity(iTaskActivity);
                return true;

            case R.id.action_genTest:
                List<Task> tmpListe = MainActivity.creerTmpListeTaches();
                for (Task t : tmpListe) {
                    t.save();
                }
                User tmpUser;
                for (int i = 1; i <= 10; i++) {
                    tmpUser = User.getUser(String.format(Locale.FRANCE, "User %d", i));
                    tmpUser.save();
                }
                chargerBD();
                rv_adapter.notifyDataSetChanged();
                return true;

            case R.id.action_task_cancel:
                rv_adapter.setModeToShow();
                return true;

            case R.id.action_task_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Suppression");
                builder.setMessage("Voulez-vous vraiment supprimer les tâches séléctionnées ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rv_adapter.deleteSelectedTask();
                        rv_adapter.notifyDataSetChanged();
                        rv_adapter.setModeToShow();
                    }
                });

                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });

                builder.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        chargerBD();
        rv_adapter.notifyDataSetChanged();
    }

    @Override
    public void setModeToSelection() {
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void setModeToShow() {
        getActivity().invalidateOptionsMenu();
    }

    public boolean isSelectedToShow() {
        if (rv_adapter != null && rv_adapter.isSelectionMode()) {
            rv_adapter.setModeToShow();
            return true;
        }
        return false;
    }
}
