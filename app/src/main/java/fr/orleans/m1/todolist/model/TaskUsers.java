package fr.orleans.m1.todolist.model;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Locale;

public class TaskUsers extends SugarRecord implements Serializable {

    private Task task;
    private User user;

    public TaskUsers() {

    }

    public static TaskUsers getTaskUsers(Task t, User u) {
        TaskUsers tu;
        Long t_id = (t != null) ? t.getId() : 0;
        Long u_id = (u != null) ? u.getId() : 0;
        if (TaskUsers.count(TaskUsers.class, "task = ? and user = ?", new String[]{t_id.toString(), u_id.toString()}) > 0) {
            tu = TaskUsers.find(TaskUsers.class, "task = ? and user = ?", new String[]{t_id.toString(), u_id.toString()}).get(0);
        } else {
            tu = new TaskUsers(t, u);
        }
        return tu;
    }

    private TaskUsers(Task t, User u) {
        task = t;
        user = u;
    }

    public Task getTask() {
        return task;
    }

    public User getUser() {
        return user;
    }

    @Override
    public long save() {
        if (user != null && task != null) {
            user.save();
            long id = super.save();
            return id;
        }
        return 0;
    }

    @Override
    public String toString() {
        return String.format(Locale.FRANCE, "%s, User:%s", (task != null) ? task.toString() : "null", (user != null) ? user.toString() : "null");
    }
}
