package fr.orleans.m1.todolist.model;

import android.text.Editable;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.io.Serializable;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

public class User extends SugarRecord implements Serializable {

    @Expose
    private String username;
    @Unique
    @Expose
    private String canonicalUsername;

    /*
        TODO: Auth
        TODO: Database
     */

    public User() {

    }

    public static User getUser(String u) {
        User user;
        if (User.count(User.class, "canonical_username = ?", new String[]{toCanonical(u)}) > 0) {
            user = User.find(User.class, "canonical_username = ?", new String[]{toCanonical(u)}).get(0);
        } else {
            user = new User(u);
        }
        return user;
    }

    private User(String username) {
        setUsername(username);
    }

    public String getUsername() {
        return username;
    }

    public String getCanonicalUsername() {
        return canonicalUsername;
    }

    public boolean setUsername(String un) {
        String canonical = toCanonical(un);
        List<User> sameUser = User.find(User.class, "canonical_username = ?", canonical);
        if (sameUser.isEmpty() || sameUser.get(0).getId() == getId()) {
            username = un;
            canonicalUsername = canonical;
            return true;
        }
        return false;
    }

    @Override
    public boolean delete() {
        boolean res = true;
        for (TaskUsers tu : TaskUsers.find(TaskUsers.class, "user = ?", getId().toString())) {
            res = res & tu.delete();
        }
        return res & super.delete();
    }

    public static String toCanonical(String text) {
        return text.toLowerCase().replaceAll("\\s+", "_");
    }

    @Override
    public String toString() {
        return canonicalUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (canonicalUsername != null && o.getClass() == getClass() && ((User)o).canonicalUsername != null) {
            return canonicalUsername.equals(((User)o).canonicalUsername);
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(canonicalUsername);
    }
}
