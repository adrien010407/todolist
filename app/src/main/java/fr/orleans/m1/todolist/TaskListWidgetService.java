package fr.orleans.m1.todolist;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;
import java.util.List;

import fr.orleans.m1.todolist.model.Task;

public class TaskListWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new TaskListRemoteViewFactory(this.getApplicationContext(), intent);
    }

    class TaskListRemoteViewFactory implements RemoteViewsFactory {
        private static final int maxCount = 10;
        private List<Task> mTasks = new ArrayList<>();

        private Context mContext;
        private int mAppWidgetId;

        public TaskListRemoteViewFactory(Context context, Intent intent) {
            mContext = context;
            mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        @Override
        public void onCreate() {
            mTasks.addAll(Task.find(Task.class, "advancement < ? and parent = ?", new String[] {"100", "0"}, "", "priority desc", Integer.toString(maxCount)));

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onDataSetChanged() {

        }

        @Override
        public void onDestroy() {
            mTasks.clear();
        }

        @Override
        public int getCount() {
            return mTasks.size();
        }

        @Override
        public RemoteViews getViewAt(int position) {
            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_task_item);
            rv.setTextViewText(R.id.title, mTasks.get(position).getName());
            int tmpRes = TaskAdapter.R_EMPTY;
            if (!mTasks.get(position).getUsers().isEmpty()) {
                tmpRes = TaskAdapter.R_PERSON_BLACK;
            }
            rv.setImageViewResource(R.id.people, tmpRes);
            Pair<Boolean, String> delay_p = mTasks.get(position).getDeadlineDelay();
            if (delay_p.first) {
                rv.setTextColor(R.id.deadline, getResources().getColor(android.R.color.holo_red_dark));
                rv.setTextViewText(R.id.deadline, "-"+delay_p.second);
            }
            else {
                rv.setTextViewText(R.id.deadline, delay_p.second);
            }
            rv.setImageViewResource(R.id.priority, mTasks.get(position).getPriorityResource());
            rv.setProgressBar(R.id.advancement, 100, mTasks.get(position).getAdvancement(), false);

            Bundle extras = new Bundle();
            extras.putLong(TodoListAppWidgetProvider.TASK_ID, mTasks.get(position).getId());
            Intent fillInIntent = new Intent();
            fillInIntent.putExtras(extras);
            rv.setOnClickFillInIntent(R.id.aTask, fillInIntent);

            return rv;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }
}
