package fr.orleans.m1.todolist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import fr.orleans.m1.todolist.model.Task;
import fr.orleans.m1.todolist.view.AdvancementBar;
import fr.orleans.m1.todolist.view.CustomLinearLayoutManager;
import fr.orleans.m1.todolist.view.DividerItemDecorator;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {
    public final static int R_PERSON_BLACK = R.drawable.ic_person_black_24dp;
    public final static int R_PERSON_BLUE = R.drawable.ic_person_blue_24dp;
    public final static int R_PEOPLES_BLACK = R.drawable.ic_people_black_24dp;
    public final static int R_PEOPLES_BLUE = R.drawable.ic_people_blue_24dp;
    public final static int R_EMPTY = R.drawable.ic_person_transparent_24dp;

    private Context context;
    private SelectableTaskActivity st;

    private List<Task> task_list;

    private boolean selection_mode = false;
    private Set<TaskViewHolder> viewHolderSet = new HashSet<>();
    private Map<Task, TaskViewHolder> selected_task_list = new HashMap<>();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ConstraintLayout mLayout;
        public TextView mTextView;
        public RecyclerView mSubTasks;
        public ImageView mPeoples;
        public ImageView mPriority;
        public TextView mDeadline;
        public AdvancementBar mAdv;
        public ToggleButton tb_toggle;

        public TaskAdapter mAdapter = null;

        public TaskViewHolder(ConstraintLayout l) {
            super(l);
            mLayout = l;
            mTextView = l.findViewById(R.id.title);
            mSubTasks = l.findViewById(R.id.subTaskList);
            tb_toggle = l.findViewById(R.id.toggle);
            mPeoples = l.findViewById(R.id.people);
            mPriority = l.findViewById(R.id.priority);
            mDeadline = l.findViewById(R.id.deadline);
            mAdv = l.findViewById(R.id.advancement);
        }
    }

    public TaskAdapter(SelectableTaskActivity st, List<Task> tl) {
        context = st.getContext();
        this.st = st;
        task_list = tl;
    }

    @NonNull
    @Override
    public TaskAdapter.TaskViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.task, viewGroup, false);

        TaskViewHolder vh = new TaskViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final TaskAdapter.TaskViewHolder viewHolder, final int pos) {
        final Task task = task_list.get(pos);

        viewHolderSet.add(viewHolder);

        viewHolder.mTextView.setText(task.getName());
        viewHolder.mAdapter = new TaskAdapter(st, task.getSubTasks());
        LinearLayoutManager llm = new CustomLinearLayoutManager(viewHolder.mLayout.getContext());
        RecyclerView.ItemDecoration did = new DividerItemDecorator(viewHolder.mSubTasks.getContext());
        viewHolder.mSubTasks.setAdapter(viewHolder.mAdapter);
        viewHolder.mSubTasks.setLayoutManager(llm);
        viewHolder.mSubTasks.addItemDecoration(did);

        int tmpRes = R_EMPTY;
        if (!task.getUsers().isEmpty()) {
            tmpRes = R_PERSON_BLACK;
        }
        viewHolder.mPeoples.setImageResource(tmpRes);

        viewHolder.mPriority.setImageResource(task.getPriorityResource());
        Pair<Boolean, String> p_deadline = task.getDeadlineDelay();
        if (p_deadline.first) {
            viewHolder.mDeadline.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
            viewHolder.mDeadline.setText("-"+p_deadline.second);
        }
        else {
            viewHolder.mDeadline.setText(p_deadline.second);
        }
        viewHolder.mAdv.setProgress(task.getAdvancement());

        viewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(task, viewHolder);
            }
        });

        viewHolder.mLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                longClick(task, viewHolder);
                return true;
            }
        });

        viewHolder.mAdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(task, viewHolder);
            }
        });

        Log.d("TaskAdapterList", String.format(Locale.FRANCE, "%s %s %d %b", task.toString(), task.getSubTasks().toString(), task.getSubTasks().size(), task.getSubTasks().isEmpty()));
        if (task.getSubTasks().isEmpty()) {
            viewHolder.tb_toggle.setEnabled(false);
        } else {
            viewHolder.tb_toggle.setEnabled(true);
        }

        viewHolder.tb_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.mSubTasks.getVisibility() == View.GONE) {
                    viewHolder.mSubTasks.setVisibility(View.VISIBLE);
                } else if (viewHolder.mSubTasks.getVisibility() == View.VISIBLE) {
                    viewHolder.mSubTasks.setVisibility(View.GONE);
                }
            }
        });
    }

    private void longClick(Task task, TaskViewHolder v) {
        Toast.makeText(context, "TaskAdapter.longclick", Toast.LENGTH_LONG).show();
        setModeToSelection();

        selected_task_list.put(task, v);
        v.mLayout.setSelected(true);
    }

    private void click(Task task, TaskViewHolder v) {
        if (selection_mode) {
            if (selected_task_list.containsKey(task)) {
                selected_task_list.remove(task);
                v.mLayout.setSelected(false);
            } else {
                selected_task_list.put(task, v);
                v.mLayout.setSelected(true);
            }
        } else {
            Intent iTaskActivity = new Intent(context, TaskActivity.class);
            iTaskActivity.putExtra("task", task.getId());

            context.startActivity(iTaskActivity);
        }
    }

    @Override
    public int getItemCount() {
        return task_list.size();
    }

    public void setModeToSelection() {
        selection_mode = true;
        for (TaskViewHolder v : viewHolderSet) {
            v.mAdapter.setModeToSelection();
        }
        selected_task_list.clear();
        st.setModeToSelection();
    }

    public void setModeToShow() {
        selection_mode = false;
        for (Task t : selected_task_list.keySet()) {
            selected_task_list.get(t).mLayout.setSelected(false);
        }
        for (TaskViewHolder v : viewHolderSet) {
            v.mAdapter.setModeToShow();
            v.mLayout.setSelected(false);
        }
        selected_task_list.clear();
        st.setModeToShow();
    }

    public void deleteSelectedTask() {
        for (TaskViewHolder v : viewHolderSet) {
            v.mAdapter.deleteSelectedTask();
        }
        for (Task t : selected_task_list.keySet()) {
            t.delete();
            task_list.remove(t);
        }
        selected_task_list.clear();
    }

    public boolean isSelectionMode() {
        boolean select_mode_childs = false;
        Iterator<TaskViewHolder> i = viewHolderSet.iterator();
        while (i.hasNext() && !select_mode_childs) {
            select_mode_childs = select_mode_childs | i.next().mAdapter.isSelectionMode();
        }
        return selection_mode | select_mode_childs;
    }

    public List<Task> getTasks() {
        return task_list;
    }

    public void setTasks(List<Task> tasks_list) {
        task_list = tasks_list;
    }
}
